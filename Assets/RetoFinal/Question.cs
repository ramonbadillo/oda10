﻿using System.Collections.Generic;
using UnityEngine;

public class Question
{
    private string _questionText;
    private string[] _answersTexts;
    private int _correctAnswer;
    private int _currentAnswers;
    private GameObject[] _anotherGameObjects;
    private GameObject _answersContainerGameObject;

    /// <summary>
    /// Question with only one correct answer
    /// </summary>
    /// <param name="questionTest">Here comes the question string</param>
    /// <param name="answerTexts">Here comes all the text of each answer</param>
    /// <param name="anotherGameObjects">Another Elements to complement the question</param>
    /// <param name="answersContainerGameObject">Set the container of your answers</param>
    /// <param name="correctAnswer">Correct Answer, NOTE it has to be int starting in 0</param>
    public Question(string questionTest, string[] answerTexts, GameObject[] anotherGameObjects, GameObject answersContainerGameObject, int correctAnswer)
    {
        _questionText = questionTest;
        _answersTexts = answerTexts;
        _correctAnswer = correctAnswer;
        _currentAnswers = 5;
        _anotherGameObjects = anotherGameObjects;
        _answersContainerGameObject = answersContainerGameObject;
    }

    public int GetCurrentAnswers()
    {
        return _currentAnswers;
    }

    public bool SetAnswer(int answerIndex)
    {
        _currentAnswers = answerIndex;
        return true;
    }

    public bool ReviewQuestion()
    {
        if (_currentAnswers != _correctAnswer)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public string GetQuestionText()
    {
        return _questionText;
    }

    public string[] GetAnserTexts()
    {
        return _answersTexts;
    }

    public void SetActiveAnotherGameObjects(bool value)
    {
        if (_anotherGameObjects != null)
        {
            foreach (GameObject currentGameObject in _anotherGameObjects)
            {
                currentGameObject.SetActive(value);
            }
        }
    }

    public GameObject GetAnswerContainer()
    {
        return _answersContainerGameObject;
    }
}