﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetoFinal : MonoBehaviour
{
    private List<Question> FullTest;
    private List<Question> Test = new List<Question>();
    private Button nextButton, backButton;

    //Question Elements
    private GameObject questionsContainerGameObject;

    private Text questionText;
    private Text questionNumberText;
    private int _questionIndex;
    private GameObject questionElementsGameObject;

    private GameObject secondQuestionGameObject,
        tirdhQuestionGameObject,
        fourthQuestionGameObject,
        fifthQuestionGameObject,
        sixthQuestionGameObject,
        seventhQuestionGameObject,
        eighthtQuestionGameObject;

    //OneAnswerElements
    private GameObject singleAnswerContainer;

    private Color answerSettedColor = Color.blue;
    private Color defaultAnswerColor = Color.black;

    //Results Elements
    private GameObject correctoPrefb, incorrectoPrefb, resultPool;

    private GameObject resultsContainer;
    private Text mainResultText;
    private Button salirRetoFinalButton;

    //Message Box Elements
    private GameObject yesNoMessageBoxGameObject;

    private Text yesNoMessageBoxText;
    private Button yesButton, noButton;

    private SoundManager soundManager;

    private void CreateRandomTest(int numberQuestions)
    {
        for (int i = 0; i < numberQuestions; i++)
        {
            int randomIndex = Random.Range(0, FullTest.Count);
            Test.Add(FullTest[randomIndex]);
            FullTest.RemoveAt(randomIndex);
        }
    }

    // Use this for initialization
    private void Start()
    {
        soundManager = transform.GetComponent<SoundManager>();
        questionsContainerGameObject = Menu.RetoFinalGameObject.transform.GetChild(0).gameObject;
        
		resultsContainer = Menu.RetoFinalGameObject.transform.GetChild(1).gameObject;
		salirRetoFinalButton = resultsContainer.transform.FindChild("btnInicio").gameObject.GetComponent<Button>();
		mainResultText = resultsContainer.transform.FindChild("mainResultText").gameObject.GetComponent<Text>();
		resultPool = resultsContainer.transform.FindChild("resultPool").gameObject;
		correctoPrefb = resultPool.transform.FindChild("correctoPrefb").gameObject;
		incorrectoPrefb = resultPool.transform.FindChild("incorrectoPrefb").gameObject;

        backButton = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(0).GetComponent<Button>();
        nextButton = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(1).GetComponent<Button>();
        questionText = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(2).GetComponent<Text>();
        questionNumberText = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>();
        singleAnswerContainer = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(3).gameObject;

        questionElementsGameObject = Menu.RetoFinalGameObject.transform.GetChild(0).GetChild(4).gameObject;
        secondQuestionGameObject = questionElementsGameObject.transform.GetChild(0).gameObject;
        tirdhQuestionGameObject = questionElementsGameObject.transform.GetChild(1).gameObject;
        fourthQuestionGameObject = questionElementsGameObject.transform.GetChild(2).gameObject;
        fifthQuestionGameObject = questionElementsGameObject.transform.GetChild(3).gameObject;
        sixthQuestionGameObject = questionElementsGameObject.transform.GetChild(4).gameObject;
        seventhQuestionGameObject = questionElementsGameObject.transform.GetChild(5).gameObject;
        eighthtQuestionGameObject = questionElementsGameObject.transform.GetChild(6).gameObject;

        yesNoMessageBoxGameObject = Menu.RetoFinalGameObject.transform.GetChild(2).gameObject;
        yesButton = yesNoMessageBoxGameObject.transform.GetChild(1).GetComponent<Button>();
        noButton = yesNoMessageBoxGameObject.transform.GetChild(2).GetComponent<Button>();
        yesNoMessageBoxText = yesNoMessageBoxGameObject.transform.GetChild(3).GetComponent<Text>();

        nextButton.onClick.AddListener(delegate ()
        {
            NextQuestion();
        });
        backButton.onClick.AddListener(delegate ()
        {
            BackQuestion();
        });

        salirRetoFinalButton.onClick.AddListener(delegate ()
        {
			this.GetComponent<SoundManager> ().playSound ("Tropical_Jazz_Paradise", 0);
			Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        });

        yesButton.onClick.AddListener(delegate ()
         {
             YesMethod();
         });

        noButton.onClick.AddListener(delegate ()
         {
             NoMethod();
         });

        //StartReto();
    }

    private void ClearReto()
    {
        questionsContainerGameObject.SetActive(true);
        resultsContainer.SetActive(false);
		nextButton.transform.GetChild(0).GetComponent<Text>().text = "SIGUIENTE";
		backButton.GetComponent<Button> ().interactable = false;
		foreach (Transform currentResult in resultPool.transform)
        {
			if(currentResult.name == "Clone")
				Destroy(currentResult.gameObject);
        }
    }

    public void StartReto()
    {
        ClearReto();
        soundManager.playSound("Sound_Fondoevaluacion", 0);
        _questionIndex = 0;
        FillTest();
        CreateRandomTest(5);
        foreach (Transform currentButtonGameObject in singleAnswerContainer.transform)
        {
            int buttonIndex = currentButtonGameObject.transform.GetSiblingIndex();
            currentButtonGameObject.gameObject.GetComponent<Button>().onClick.AddListener(delegate ()
            {
                SetAnswer(buttonIndex, singleAnswerContainer);
            });
        }
        SetQuestions(0);
    }

    private void YesMethod()
    {
        if (_questionIndex <= 0)
        {
            yesNoMessageBoxGameObject.SetActive(false);
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.Menu);
        }
        else
        {
            yesNoMessageBoxGameObject.SetActive(false);
            EvaluateTest();
        }
    }

    private void NoMethod()
    {
        yesNoMessageBoxGameObject.SetActive(false);
    }

    private void ShowMessageBox(string Message)
    {
        yesNoMessageBoxText.text = Message;
        yesNoMessageBoxGameObject.SetActive(true);
    }

    private void SetAnswer(int answerIndex, GameObject answerContainerGameObject)
    {
        soundManager.playSound("Sound_SeleccionarRespuesta", 1);
        Test[_questionIndex].SetAnswer(answerIndex);
        foreach (Transform currentButtonGameObject in answerContainerGameObject.transform)
        {
            if (currentButtonGameObject.transform.GetSiblingIndex() == answerIndex)
            {
                currentButtonGameObject.gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().color = answerSettedColor;
            }
            else
            {
                currentButtonGameObject.gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().color = defaultAnswerColor;
            }
        }
    }

    private Vector3 nextButtonPosition = new Vector3(265, -207);

    private void NextQuestion()
    {
        if (_questionIndex >= Test.Count - 1)
        {
            questionsContainerGameObject.SetActive(false);
            EvaluateTest();
        }
        else
        {
            if (_questionIndex >= Test.Count - 2)
            {
                nextButton.transform.GetChild(0).GetComponent<Text>().text = "TERMINAR";
            }
            else
            {
				nextButton.transform.GetChild(0).GetComponent<Text>().text = "SIGUIENTE";

                backButton.transform.GetChild(0).GetComponent<Text>().text = "ANTERIOR";
				backButton.GetComponent<Button> ().interactable = true;
            }
            _questionIndex = Mathf.Clamp(_questionIndex + 1, 0, Test.Count - 1);

            SetQuestions(_questionIndex);
        }
    }

    private void BackQuestion()
    {
        if (_questionIndex <= 1)
        {
            backButton.transform.GetChild(0).GetComponent<Text>().text = "ANTERIOR";
			backButton.GetComponent<Button> ().interactable = false;
        }
        else
        {
			backButton.GetComponent<Button> ().interactable = true;
            nextButton.GetComponent<RectTransform>().localPosition = nextButtonPosition;
            nextButton.transform.GetChild(0).GetComponent<Text>().text = "SIGUIENTE";
        }

        _questionIndex = Mathf.Clamp(_questionIndex - 1, 0, Test.Count - 1);
        SetQuestions(_questionIndex);
    }

    private void SetQuestions(int questionIndex)
    {
        foreach (Transform currentGameObject in questionElementsGameObject.transform)
        {
            currentGameObject.gameObject.SetActive(false);
        }

        questionNumberText.text = questionIndex + 1 + ".";
        questionText.text = Test[questionIndex].GetQuestionText();
        Test[questionIndex].SetActiveAnotherGameObjects(true);
        GameObject answerContainerGameObject = Test[questionIndex].GetAnswerContainer();
        int indexAnswer = 0;

        foreach (string answerString in Test[questionIndex].GetAnserTexts())
        {
            answerContainerGameObject.transform.GetChild(indexAnswer).GetChild(0).GetComponent<Text>().text = answerString;
            indexAnswer++;
        }

        if (Test[questionIndex].GetCurrentAnswers() > 0)
        {
            SetAnswer(Test[questionIndex].GetCurrentAnswers(), answerContainerGameObject);
        }
        else
        {
            CleanAnswers(answerContainerGameObject);
        }
    }

    private void CleanAnswers(GameObject answerContainerGameObject)
    {
        foreach (Transform currentButtonGameObject in answerContainerGameObject.transform)
        {
            currentButtonGameObject.gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().color = defaultAnswerColor;
        }
    }

    private void EvaluateTest()
    {
        GameObject resultGameObject;
        int questionPositionIndex = 0;
        int rigthQuestions = 0;
        foreach (Question currentQuestion in Test)
        {
            
			if (currentQuestion.ReviewQuestion ()) {
				resultGameObject = (GameObject)Instantiate (correctoPrefb);
				rigthQuestions++;
			} else {
				resultGameObject = (GameObject)Instantiate (incorrectoPrefb);
			}
			resultGameObject.SetActive (true);
			resultGameObject.name = "Clone";
			resultGameObject.transform.SetParent(resultPool.transform);
			resultGameObject.GetComponent<RectTransform>().localPosition = 
				resultGameObject.transform.position + ((Vector3.down) * questionPositionIndex * 34);
			resultGameObject.transform.localScale = new Vector3(1, 1, 1);
            questionPositionIndex++;
        }
        mainResultText.text = " " + rigthQuestions * 2 + "/" + Test.Count * 2;
        resultsContainer.SetActive(true);
    }

    private void FillTest()
    {
        FullTest = new List<Question>();
        FullTest.Clear();
        Test.Clear();

        FullTest.Add(new Question("¿Qué diferencia existe entre un poema y un cuento?",
                               new string[] { "El poema se escribe en versos", "El cuento se escribe en versos", "Ninguna" },
                               null,
                               singleAnswerContainer,
                               0));
        FullTest.Add(new Question("¿A qué tipo de texto pertenecen estas líneas?\n “Había una vez un comerciante que vivía en un pueblito lejano, tenía una tienda de abarrotes y los vecinos le decían Lalo. Tenía mal genio, no le gustaba platicar con nadie y siempre se enojaba con los niños.” ",
                               new string[] { "Teatro", "Cuento", "Poema" },
                               null,
                               singleAnswerContainer,
                               1));
        FullTest.Add(new Question("¿A qué tipo de texto pertenecen estas líneas?\n Nada te turbe,\n Nada te espante,\n Todo se pasa.\n Dios no se muda.",
                               new string[] { "Teatro", "Cuento", "Poema" },
                               null,
                               singleAnswerContainer,
                               2));
        FullTest.Add(new Question("Para recomendar un libro es mejor:",
                               new string[] { "preguntar por el tema que le interesa al lector", "proponer un libro al azar", "proponer poemas" },
                               null,
                               singleAnswerContainer,
                               0));
        FullTest.Add(new Question("¿A qué tipo de texto pertenecen estas líneas?\n La tortuga Pocaprisa\n tiene su modo de andar:\n camina poco y se para\n a ver el viento pasar.",
                               new string[] { "Teatro", "Novela", "Poema" },
                               null,
                               singleAnswerContainer,
                               2));
        FullTest.Add(new Question("Un texto de teatro tiene:",
                               new string[] { "Versos", "Diálogos", "Rimas" },
                               null,
                               singleAnswerContainer,
                               1));
        FullTest.Add(new Question("¿A qué tipo de texto pertenecen estas líneas?\n Ahora hace ya seis años de esto. Jamás he contado esta historia y los compañeros que me vuelven a ver se alegran de encontrarme vivo.",
                               new string[] { "Teatro", "Novela", "Poema" },
                               null,
                               singleAnswerContainer,
                               1));
    }
}