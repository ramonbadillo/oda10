﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Reto2 : MonoBehaviour
{
    private Text animatedText;
    private GameObject Screen1, Screen2;

    private Button nextStepButton, skipButton;
    private int currentStepIndex = 0;
    private int characterIndex = 0;
    private int numberOfTrys = 0;
	private int numTrofeos = 0;
    private string fullAnimatedText = "";

    private List<Option> currentOptions;
    private List<Option> Options1 = new List<Option>();
    private List<Option> Options2 = new List<Option>();
    private GameObject optionsContainer, currentBookContainer, currentOptionContainter, currentOptionBackground;
    private GameObject haceMuchos, estabaSentado, riendoVoy, habeisSido, tieneLos, cocinaAmplia;
    private List<Book> currentBooks;
    private List<Book> Books1 = new List<Book>();
    private List<Book> Books2 = new List<Book>();
    private GameObject booksContainer;
    private GameObject soldado, amiga, arboles, agua, nina, buque;
    private GameObject currentBook, currentAnswer;
	private GameObject containterScrollView, contadorGameObject, TrofeoGameObject;

	private GameObject popUpContainer;
	private Button popUpContainerButton;
	private Text popUpText, contadorText;

    // Use this for initialization
    private void Start()
    {
        Screen1 = Menu.Reto2GameObject.transform.GetChild(0).gameObject;
        Screen2 = Menu.Reto2GameObject.transform.GetChild(1).gameObject;
        animatedText = Screen1.transform.FindChild("bibliotecaria").GetChild(0).GetChild(0).GetComponent<Text>();
        nextStepButton = Screen1.transform.FindChild("NextButton").GetComponent<Button>();
        skipButton = Screen1.transform.FindChild("SkipButton").GetComponent<Button>();
        containterScrollView = Screen2.transform.FindChild("ScrollContainer").GetChild(0).GetChild(0).GetChild(0).gameObject;
        currentBookContainer = Screen2.transform.FindChild("CurrentContainer").FindChild("Book").gameObject;
        currentOptionContainter = Screen2.transform.FindChild("CurrentContainer").FindChild("Option").gameObject;
        currentOptionBackground = Screen2.transform.FindChild("CurrentContainer").FindChild("BackgroundOption").gameObject;
		popUpContainer = Screen2.transform.FindChild ("PopUpContainer").gameObject;
		popUpContainerButton = popUpContainer.transform.FindChild("Button").GetComponent<Button>();
        popUpText = Screen2.transform.FindChild("PopUpContainer").FindChild("Text").gameObject.GetComponent<Text>();

        booksContainer = Screen2.transform.FindChild("BooksPrefabsContainer").gameObject;
        soldado = booksContainer.transform.FindChild("soldado").gameObject;
        amiga = booksContainer.transform.FindChild("amiga").gameObject;
        arboles = booksContainer.transform.FindChild("arboles").gameObject;
        agua = booksContainer.transform.FindChild("agua").gameObject;
        nina = booksContainer.transform.FindChild("nina").gameObject;
        buque = booksContainer.transform.FindChild("buque").gameObject;

        optionsContainer = Screen2.transform.FindChild("OptionsPrefabsContainer").gameObject;
        haceMuchos = optionsContainer.transform.FindChild("haceMuchos").gameObject;
        estabaSentado = optionsContainer.transform.FindChild("estabaSentado").gameObject;
        riendoVoy = optionsContainer.transform.FindChild("riendoVoy").gameObject;
        habeisSido = optionsContainer.transform.FindChild("habeisSido").gameObject;
        tieneLos = optionsContainer.transform.FindChild("tieneLos").gameObject;
        cocinaAmplia = optionsContainer.transform.FindChild("cocinaAmplia").gameObject;

		contadorGameObject = Screen2.transform.FindChild("contador").gameObject;
		contadorText = contadorGameObject.transform.FindChild("contadorText").GetComponent<Text>();

		TrofeoGameObject = Screen2.transform.FindChild("Trofeo").gameObject;

        nextStepButton.onClick.AddListener(delegate ()
            {
                NextButtonMethod();
            });

        skipButton.onClick.AddListener(delegate ()
            {
                SkipButtonMethod();
            });

        popUpContainerButton.onClick.AddListener(delegate ()
        {
            HideMessageBox();
        });

        foreach (Transform currentOption in optionsContainer.transform)
        {
            currentOption.gameObject.AddComponent<DragMeReto2>();
        }
        currentOptionContainter.AddComponent<DropMeReto2>();
        currentOptionContainter.GetComponent<DropMeReto2>().containerImage =
            currentOptionBackground.GetComponent<Image>();
        currentOptionContainter.GetComponent<DropMeReto2>().receivingImage =
            currentOptionContainter.GetComponent<Image>();
        FillQuestions();

        //StartReto();
    }

    private void ClearReto()
    {
        Screen2.SetActive(false);

        foreach (Transform currentBook in booksContainer.transform)
        {
            currentBook.localPosition = Vector3.zero;
        }
        haceMuchos.transform.SetParent(optionsContainer.transform);
        estabaSentado.transform.SetParent(optionsContainer.transform);
        riendoVoy.transform.SetParent(optionsContainer.transform);
        habeisSido.transform.SetParent(optionsContainer.transform);
        tieneLos.transform.SetParent(optionsContainer.transform);
        cocinaAmplia.transform.SetParent(optionsContainer.transform);
        foreach (Transform currentOption in optionsContainer.transform)
        {
            currentOption.localPosition = Vector3.zero;
        }
			
		if (PlayerPrefs.HasKey ("oda10_trofeos")) {
			numTrofeos = PlayerPrefs.GetInt ("oda10_trofeos");
		} else {
			numTrofeos = 0;
		}
		contadorText.text = ""+numTrofeos;
        //currentBooks.Clear();
        //currentOptions.Clear();
    }

    private int currentBookIndex = 0;

    public void StartReto()
    {
        numberOfTrys = 0;
        currentBookIndex = 0;
        ClearReto();
        if (Random.Range(0, 2) == 1)
        {
            currentBooks = Books1;
            currentOptions = Options1;
        }
        else
        {
            currentBooks = Books2;
            currentOptions = Options2;
        }
        
        currentBooks.Shuffle();
        currentOptions.Shuffle();
        currentBooks[currentBookIndex].bookGameObject.transform.SetParent(currentBookContainer.transform);
        currentBooks[currentBookIndex].bookGameObject.transform.localPosition = Vector3.zero;
        currentBooks[currentBookIndex].bookGameObject.transform.localScale = Vector3.one;
        for (int i = 0; i < currentOptions.Count; i++)
        {
            currentOptions[i].optionGameObject.transform.SetParent(containterScrollView.transform.GetChild(i));
            currentOptions[i].optionGameObject.transform.localPosition = Vector3.zero;
            currentOptions[i].optionGameObject.transform.localScale = Vector3.one;
        }

        currentStepIndex = 0;

		if (PlayerPrefs.HasKey ("oda10_trofeos")) {
			contadorText.text = ""+PlayerPrefs.GetInt ("oda10_trofeos");
		} else {
			contadorText.text = "0";
		}
        MainMethod();
    }

    private void SetPositions()
    {
    }

    private IEnumerator AnimateText(string text, float speed)
    {
        skipButton.gameObject.SetActive(true);
        fullAnimatedText = text;
        animatedText.text = "";
        characterIndex = 0;
        while (true)
        {
            yield return new WaitForSeconds(speed);
            if (characterIndex > text.Length)
            {
                currentStepIndex++;
                MainMethod();
                break;
            }
            animatedText.text = text.Substring(0, characterIndex);
            characterIndex++;
        }
    }

    private void MainMethod()
    {
        switch (currentStepIndex)
        {
            case 0:
				StartCoroutine(AnimateText("¡Los textos se mezclaron! Ayúdame a regresar cada texto con su título.", 0.15f));
                break;

            case 1:
                skipButton.gameObject.SetActive(false);
                nextStepButton.gameObject.SetActive(true);
                break;

            case 2:

                Screen2.SetActive(true);
                break;

            case 3:

                break;
        }
    }

    private void SkipButtonMethod()
    {
        characterIndex = fullAnimatedText.Length;
        animatedText.text = fullAnimatedText;
        skipButton.gameObject.SetActive(false);
    }

    private void NextButtonMethod()
    {
        currentStepIndex++;
        MainMethod();
        nextStepButton.gameObject.SetActive(false);
    }

    private void FillQuestions()
    {
        Books1.Add(
            new Book(
                buque,
				"haceMuchos",
				"No hay relación entre el texto y el título, ¿cuál texto contiene elementos que se relacionan con un buque fantasma y cierto misterio?",
				"Por la referencia a un marino y a un viaje, este texto corresponde al Buque fantasma. ¡Sigamos con el tercer reto!"
            )
        );
        Books1.Add(
            new Book(
                agua,
				"riendoVoy",
				"No hay relación entre el texto y el título, ¿cuál texto contiene elementos que se relacionan con el agua?",
				"Por la referencia al río, al mar y la pregunta “¿adónde vas?”,  este texto corresponde a  Agua, ¿dónde vas? ¡Sigamos con el tercer reto!"
            )
        );
        Books1.Add(
            new Book(
                nina,
				"tieneLos",
				"No hay relación entre el texto y el título, ¿cuál texto contiene elementos que se relacionan con una persona agradable?",
				"Se describe a alguien de forma agradable, podemos pensar que se describe a la niña, este texto corresponde a La niña que yo más quiero. ¡Sigamos con el tercer reto!"
            )
        );

        Options1.Add(
            new Option(
                haceMuchos,
                "haceMuchos"
            )
        );

        Options1.Add(
            new Option(
                riendoVoy,
                "riendoVoy"
            )
        );
        Options1.Add(
            new Option(
                tieneLos,
                "tieneLos"
            )
        );
        Options1.Add(
            new Option(
                estabaSentado,
                "estabaSentado"
            )
        );

        Books2.Add(
            new Book(
                soldado,
				"estabaSentado",
				"No hay relación entre el texto y el título, ¿cuál texto contiene elementos que se relacionan con la guerra?",
				"Por la referencia a la guerra,  este texto corresponde a El regreso de un soldado. ¡Sigamos con el tercer reto!"
            )
        );
        Books2.Add(
            new Book(
                arboles,
				"habeisSido",
				"No hay relación entre el texto y el título, ¿cuál texto contiene elementos que se relacionan con los árboles?",
				"En el texto aparece la palabra “árboles”, este texto corresponde a  Árboles. ¡Sigamos con el tercer reto!"
            )
        );
        Books2.Add(
            new Book(
                amiga,
				"cocinaAmplia",
				"No hay relación entre el texto y el título, ¿cuál texto contiene elementos que se relacionan con la amistad?",
				" En el texto aparecen dos niñas que parecen llevarse bien y que podrían ser amigas, este texto corresponde a Mi mejor amiga. ¡Sigamos con el tercer reto!"
            )
        );

        Options2.Add(
            new Option(
                estabaSentado,
                "estabaSentado"
            )
        );
        Options2.Add(
            new Option(
                habeisSido,
                "habeisSido"
            )
        );
        Options2.Add(
            new Option(
                cocinaAmplia,
                "cocinaAmplia"
            )
        );
        Options2.Add(
            new Option(
                haceMuchos,
                "haceMuchos"
            )
        );
    }

    public bool CheckOption(string optionName)
    {
        int indexOptionName = 0;
        for (int i = 0; i < currentOptions.Count; i++)
        {
            if (currentOptions[i].gameObjectName == optionName)
            {
                indexOptionName = i;
            }
        }

        if (currentBooks[currentBookIndex].correctAnswer == optionName)
        {
            currentOptions[indexOptionName].optionGameObject.transform.SetParent(optionsContainer.transform);
            currentOptions[indexOptionName].optionGameObject.transform.localPosition = Vector3.zero;
            
			StartCoroutine (mostrarTrofeo ());

            numberOfTrys = 0;

            return true;
        }
        else
        {
			this.GetComponent<SoundManager> ().playSound ("Sound_incorrecto_19", 1);
			if (numberOfTrys == 0)
            {
				ShowMessageBox(currentBooks[currentBookIndex].firstFail);
                numberOfTrys++;
            }
            else if (numberOfTrys == 1)
            {
				numberOfTrys++;
				if (currentBookIndex == currentBooks.Count - 1) {
					ShowMessageBox (currentBooks[currentBookIndex].secondFail);
					numberOfTrys = 0;
				} else {
					ShowMessageBox(currentBooks[currentBookIndex].firstFail);
					currentBooks[currentBookIndex].bookGameObject.transform.SetParent(booksContainer.transform);
					currentBooks[currentBookIndex].bookGameObject.transform.localPosition = Vector3.zero;
					currentBookIndex++;
					currentBooks[currentBookIndex].bookGameObject.transform.SetParent(currentBookContainer.transform);
					currentBooks[currentBookIndex].bookGameObject.transform.localPosition = Vector3.zero;
				}
					
            }

            return false;
        }
    }

	//Mostrar trofeo
	IEnumerator mostrarTrofeo()
	{
		TrofeoGameObject.SetActive (true);
		numTrofeos++;
		contadorGameObject.SetActive (true);
		contadorText.text = ""+numTrofeos;
		PlayerPrefs.SetInt ("oda10_trofeos",numTrofeos);
		yield return new WaitForSeconds (1.0f);
		TrofeoGameObject.SetActive (false);
		if (currentBookIndex != currentBooks.Count - 1)
		{
			this.GetComponent<SoundManager> ().playSound ("Sound_correcto_10", 1);
			currentBooks[currentBookIndex].bookGameObject.transform.SetParent(booksContainer.transform);
			currentBooks[currentBookIndex].bookGameObject.transform.localPosition = Vector3.zero;
			currentBookIndex++;
			currentBooks[currentBookIndex].bookGameObject.transform.SetParent(currentBookContainer.transform);
			currentBooks[currentBookIndex].bookGameObject.transform.localPosition = Vector3.zero;
		}
		else
		{
			this.GetComponent<SoundManager> ().playSound ("TerminarReto", 1);
			ShowMessageBox("¡Sigamos con el tercer reto!");
		}
	}

    private void HideMessageBox()
    {
		this.GetComponent<SoundManager> ().playSound ("Sound_Cerrar_3", 1);
        
        currentOptionContainter.GetComponent<Image>().color = Color.clear;
		if (currentBookIndex == currentBooks.Count - 1 && numberOfTrys == 0)
        {
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.Reto3);
            gameObject.GetComponent<Reto3>().StartReto();
        }
		if(numberOfTrys == 2){
			numberOfTrys = 0;
		}
        popUpContainer.SetActive(false);

		contadorText.text = ""+PlayerPrefs.GetInt ("oda10_trofeos");
    }

    private void ShowMessageBox(string message)
    {
        popUpContainer.SetActive(true);
        popUpText.text = message;
        //print(message);
    }
}

internal static class MyExtensions
{
    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}

public class Option
{
    public GameObject optionGameObject;
    public string gameObjectName;
    

    public Option(GameObject optionGameObject, string gameObjectName)
    {
        this.optionGameObject = optionGameObject;
        this.gameObjectName = gameObjectName;
        
    }
}

public class Book
{
    public GameObject bookGameObject;
    public string correctAnswer;
	public string firstFail;
	public string secondFail;

	public Book(GameObject bookGameObject, string correctAnswer, string firstFail, string secondFail)
	{
   
        this.bookGameObject = bookGameObject;
        this.correctAnswer = correctAnswer;
		this.firstFail = firstFail;
		this.secondFail = secondFail;
    }
}

[RequireComponent(typeof(Image))]
public class DragMeReto2 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public bool dragOnSurfaces = false;

    private Dictionary<int, GameObject> m_DraggingIcons = new Dictionary<int, GameObject>();
    private Dictionary<int, RectTransform> m_DraggingPlanes = new Dictionary<int, RectTransform>();

    public void OnBeginDrag(PointerEventData eventData)
    {
        var canvas = FindInParents<Canvas>(gameObject);
        if (canvas == null)
            return;

        // We have clicked something that can be dragged.
        // What we want to do is create an icon for this.
        m_DraggingIcons[eventData.pointerId] = new GameObject("icon");

        m_DraggingIcons[eventData.pointerId].transform.SetParent(canvas.transform, false);
        m_DraggingIcons[eventData.pointerId].transform.SetAsLastSibling();

        var image = m_DraggingIcons[eventData.pointerId].AddComponent<Image>();
        // The icon will be under the cursor.
        // We want it to be ignored by the event system.
        var group = m_DraggingIcons[eventData.pointerId].AddComponent<CanvasGroup>();
        group.blocksRaycasts = false;

        image.sprite = GetComponent<Image>().sprite;
        image.SetNativeSize();

        if (dragOnSurfaces)
            m_DraggingPlanes[eventData.pointerId] = transform as RectTransform;
        else
            m_DraggingPlanes[eventData.pointerId] = canvas.transform as RectTransform;

        SetDraggedPosition(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        GetComponent<Image>().color = Color.clear;
        if (m_DraggingIcons[eventData.pointerId] != null)
            SetDraggedPosition(eventData);
    }

    private void SetDraggedPosition(PointerEventData eventData)
    {
        if (dragOnSurfaces && eventData.pointerEnter != null && eventData.pointerEnter.transform as RectTransform != null)
            m_DraggingPlanes[eventData.pointerId] = eventData.pointerEnter.transform as RectTransform;

        var rt = m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>();
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlanes[eventData.pointerId], eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
            rt.rotation = m_DraggingPlanes[eventData.pointerId].rotation;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (m_DraggingIcons[eventData.pointerId] != null)
        {
            Destroy(m_DraggingIcons[eventData.pointerId]);
        }

        GetComponent<Image>().color = Color.white;
        m_DraggingIcons[eventData.pointerId] = null;
    }

    static public T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        var t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }
}

public class DropMeReto2 : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Image containerImage;
    public Image receivingImage;
    private Color normalColor = Color.white;
    public Color highlightColor = Color.yellow;

    public void OnDrop(PointerEventData data)
    {
        containerImage.color = normalColor;

        if (receivingImage == null)
            return;

        Sprite dropSprite = GetDropSprite(data);
        if (dropSprite != null)
        {
            if (!Menu.ParentObject.GetComponent<Reto2>().CheckOption(dropSprite.name))
            {
                receivingImage.overrideSprite = dropSprite;
            }
            else
            {
                receivingImage.sprite = null;
            }
            //print(dropSprite.name);
        }
    }

    public void OnPointerEnter(PointerEventData data)
    {
        if (containerImage == null)
            return;

        Sprite dropSprite = GetDropSprite(data);
        if (dropSprite != null)
            containerImage.color = highlightColor;
    }

    public void OnPointerExit(PointerEventData data)
    {
        if (containerImage == null)
            return;

        containerImage.color = Color.white;
    }

    private Sprite GetDropSprite(PointerEventData data)
    {
        var originalObj = data.pointerDrag;
        if (originalObj == null)
            return null;

        var dragMe = originalObj.GetComponent<DragMeReto2>();
        if (dragMe == null)
            return null;

        var srcImage = originalObj.GetComponent<Image>();
        if (srcImage == null)
            return null;

        return srcImage.sprite;
    }
}