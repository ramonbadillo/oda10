﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Reto3 : MonoBehaviour
{
    private Text animatedText;
    private GameObject Screen1, Screen2;

    private Button nextStepButton, skipButton;
    private int currentStepIndex = 0;
    private int characterIndex = 0;

    private bool[] currentAnswers = new bool[] { false, false, false };
    private List<OptionReto3> currentOptions;
    private List<OptionReto3> Options1 = new List<OptionReto3>();
    private List<OptionReto3> Options2 = new List<OptionReto3>();
    private GameObject teatro, cuento, poesia;
    private GameObject OptionTextContainer, agua, buque, nina, habeis, cocina, estaba;

    private GameObject dragOption1, dragOption2, dragOption3;
    private GameObject dropOption1, dropOption2, dropOption3;
    private GameObject dropOptionBox1, dropOptionBox2, dropOptionBox3;
	private GameObject popUpContainer;
	private Button popUpContainerButton;
	private Text popUpText, contadorText;
	private GameObject TrofeoGameObject, contadorGameObject;

	private int numTrofeos = 0;
	private int numIntentos = 0;

    // Use this for initialization
    private void Start()
    {
        Screen1 = Menu.Reto3GameObject.transform.GetChild(0).gameObject;
        Screen2 = Menu.Reto3GameObject.transform.GetChild(1).gameObject;
        animatedText = Screen1.transform.FindChild("bibliotecaria").GetChild(0).GetChild(0).GetComponent<Text>();
        nextStepButton = Screen1.transform.FindChild("NextButton").GetComponent<Button>();
        skipButton = Screen1.transform.FindChild("SkipButton").GetComponent<Button>();

		TrofeoGameObject = Screen2.transform.FindChild("Trofeo").gameObject;
		contadorGameObject = Screen2.transform.FindChild("contador").gameObject;
		contadorText = contadorGameObject.transform.FindChild("contadorText").GetComponent<Text>();

        teatro = Screen2.transform.FindChild("TopPanel").FindChild("DragoBoxTeatro").FindChild("Teatro").gameObject;
        cuento = Screen2.transform.FindChild("TopPanel").FindChild("DragoBoxCuento").FindChild("Cuento").gameObject;
        poesia = Screen2.transform.FindChild("TopPanel").FindChild("DragoBoxPoesia").FindChild("Poesia").gameObject;

        dragOption1 = Screen2.transform.FindChild("TopPanel").FindChild("DragoBoxTeatro").FindChild("Teatro").gameObject;
        dragOption2 = Screen2.transform.FindChild("TopPanel").FindChild("DragoBoxCuento").FindChild("Cuento").gameObject;
        dragOption3 = Screen2.transform.FindChild("TopPanel").FindChild("DragoBoxPoesia").FindChild("Poesia").gameObject;

        dragOption1.AddComponent<DragMeReto3>();
        dragOption1.GetComponent<DragMeReto3>().CurrentAnswer = "TEATRO";
        dragOption2.AddComponent<DragMeReto3>();
        dragOption2.GetComponent<DragMeReto3>().CurrentAnswer = "CUENTO";
        dragOption3.AddComponent<DragMeReto3>();
        dragOption3.GetComponent<DragMeReto3>().CurrentAnswer = "POESIA";

        dropOption1 = Screen2.transform.FindChild("Panel1").FindChild("DropOption").gameObject;
        dropOption2 = Screen2.transform.FindChild("Panel2").FindChild("DropOption").gameObject;
        dropOption3 = Screen2.transform.FindChild("Panel3").FindChild("DropOption").gameObject;
        dropOptionBox1 = Screen2.transform.FindChild("Panel1").FindChild("DropBox").FindChild("DropImage").gameObject;
        dropOptionBox2 = Screen2.transform.FindChild("Panel2").FindChild("DropBox").FindChild("DropImage").gameObject;
        dropOptionBox3 = Screen2.transform.FindChild("Panel3").FindChild("DropBox").FindChild("DropImage").gameObject;

		popUpContainer = Screen2.transform.FindChild("PopUpContainer").gameObject;
		popUpContainerButton = popUpContainer.transform.FindChild("Button").GetComponent<Button>();
        popUpText = Screen2.transform.FindChild("PopUpContainer").FindChild("Text").gameObject.GetComponent<Text>();

        dropOptionBox1.AddComponent<DropMeReto3>();
        dropOptionBox1.GetComponent<DropMeReto3>().receivingImage = dropOptionBox1.GetComponent<Image>();
        dropOptionBox2.AddComponent<DropMeReto3>();
        //dropOptionBox2.GetComponent<DropMeReto3>().containerImage = dropOptionBox2.GetComponent<Image>();
        dropOptionBox2.GetComponent<DropMeReto3>().receivingImage = dropOptionBox2.GetComponent<Image>();
        dropOptionBox3.AddComponent<DropMeReto3>();
        //dropOptionBox3.GetComponent<DropMeReto3>().containerImage = dropOptionBox3.GetComponent<Image>();
        dropOptionBox3.GetComponent<DropMeReto3>().receivingImage = dropOptionBox3.GetComponent<Image>();

        OptionTextContainer = Screen2.transform.FindChild("OptionTextContainer").gameObject;
        agua = OptionTextContainer.transform.FindChild("agua").gameObject;
        buque = OptionTextContainer.transform.FindChild("buque").gameObject;
        nina = OptionTextContainer.transform.FindChild("nina").gameObject;
        habeis = OptionTextContainer.transform.FindChild("habeis").gameObject;
        cocina = OptionTextContainer.transform.FindChild("cocina").gameObject;
        estaba = OptionTextContainer.transform.FindChild("estaba").gameObject;

        nextStepButton.onClick.AddListener(delegate ()
        {
            NextButtonMethod();
        });

        skipButton.onClick.AddListener(delegate ()
        {
            SkipButtonMethod();
        });

        popUpContainerButton.onClick.AddListener(delegate ()
        {
            HideMessageBox();
        });

        FillQuestions();
        //StartReto();
    }

    private void ClearReto()
    {
        dropOptionBox1.GetComponent<Image>().color = Color.black;
        dropOptionBox2.GetComponent<Image>().color = Color.black;
        dropOptionBox3.GetComponent<Image>().color = Color.black;
        Screen2.SetActive(false);
        agua.transform.SetParent(OptionTextContainer.transform);
        buque.transform.SetParent(OptionTextContainer.transform);
        nina.transform.SetParent(OptionTextContainer.transform);
        habeis.transform.SetParent(OptionTextContainer.transform);
        cocina.transform.SetParent(OptionTextContainer.transform);
        estaba.transform.SetParent(OptionTextContainer.transform);
        foreach (Transform currentOption in OptionTextContainer.transform)
        {
            currentOption.transform.localPosition = Vector3.zero;
        }
        failedGame = false;
        winGame = false;
		currentAnswers = new bool[] { false, false, false };

		currentStepIndex = 0;
		if (PlayerPrefs.HasKey ("oda10_trofeos")) {
			numTrofeos = PlayerPrefs.GetInt ("oda10_trofeos");
		} else {
			numTrofeos = 0;
		}
		contadorText.text = ""+numTrofeos;
		contadorGameObject.SetActive (false);

		numIntentos = 0;
    }

    public void StartReto()
    {
        currentStepIndex = 0;
        ClearReto();
        if (Random.Range(0, 2) == 1)
        {
            currentOptions = Options1;
        }
        else
        {
            currentOptions = Options2;
        }

        currentOptions[0].optionGameObject.transform.SetParent(dropOption1.transform);
        dropOptionBox1.GetComponent<DropMeReto3>().currentQuestion = 0;
        currentOptions[0].optionGameObject.transform.localPosition = Vector3.zero;
        currentOptions[0].optionGameObject.transform.localScale = Vector3.one;
        currentOptions[1].optionGameObject.transform.SetParent(dropOption2.transform);
        dropOptionBox2.GetComponent<DropMeReto3>().currentQuestion = 1;
        currentOptions[1].optionGameObject.transform.localPosition = Vector3.zero;
        currentOptions[1].optionGameObject.transform.localScale = Vector3.one;
        currentOptions[2].optionGameObject.transform.SetParent(dropOption3.transform);
        dropOptionBox3.GetComponent<DropMeReto3>().currentQuestion = 2;
        currentOptions[2].optionGameObject.transform.localPosition = Vector3.zero;
        currentOptions[2].optionGameObject.transform.localScale = Vector3.one;

        MainMethod();

    }

    private string fullAnimatedText = "";

    private IEnumerator AnimateText(string text, float speed)
    {
        skipButton.gameObject.SetActive(true);
        fullAnimatedText = text;
        animatedText.text = "";
        characterIndex = 0;
        while (true)
        {
            yield return new WaitForSeconds(speed);
            if (characterIndex > text.Length)
            {
                currentStepIndex++;
                MainMethod();
                break;
            }
            animatedText.text = text.Substring(0, characterIndex);
            characterIndex++;
        }
    }

    private void MainMethod()
    {
        switch (currentStepIndex)
        {
            case 0:
                StartCoroutine(
                    AnimateText(
                        "¡Estoy perdida! Ayúdame a identificar a qué tipo de textos pertenecen los textos que salvamos.",
                        0.15f));
                break;

            case 1:
                skipButton.gameObject.SetActive(false);
                nextStepButton.gameObject.SetActive(true);
                break;

            case 2:
                Screen2.SetActive(true);
				this.GetComponent<SoundManager> ().playSound ("Sound_OpenPopUp_10", 1);
                ShowMessageBox(" Vuelve a leer los textos e identifica a qué tipo pertenecen. Arrastra la etiqueta adecuada.");
                break;

            case 3:
                break;
        }
    }

    private void SkipButtonMethod()
    {
        characterIndex = fullAnimatedText.Length;
        animatedText.text = fullAnimatedText;
        skipButton.gameObject.SetActive(false);
    }

    private void NextButtonMethod()
    {
        currentStepIndex++;
        MainMethod();
        nextStepButton.gameObject.SetActive(false);
    }

    private void FillQuestions()
    {
        Options1.Add(
            new OptionReto3(
                agua,
                "POESIA",
                "Es un poema porque el texto se compone de versos. Fíjate en la forma y en el ritmo del texto. ¡Sigamos con el cuarto reto!"
            )
        );
        Options1.Add(
            new OptionReto3(
                buque,
                "CUENTO",
                "Es un cuento porque cuenta una historia. ¡Sigamos con el cuarto reto!"
            )
        );
        Options1.Add(
            new OptionReto3(
                nina,
                "POESIA",
                "Es un poema porque el texto se compone de versos. Fíjate en la forma y en el ritmo del texto. ¡Sigamos con el cuarto reto!"
            )
        );
        Options2.Add(
            new OptionReto3(
                estaba,
                "CUENTO",
                "Es un cuento porque cuenta una historia. ¡Sigamos con el cuarto reto!"
            )
        );
        Options2.Add(
            new OptionReto3(
                habeis,
                "POESIA",
                "Es un poema porque el texto se compone de versos. Fíjate en la forma y en el ritmo del texto. ¡Sigamos con el cuarto reto!"
            )
        );
        Options2.Add(
            new OptionReto3(
                cocina,
                "TEATRO",
                "Es un texto teatral porque hay diálogos, se describe la actitud de los personajes y el escenario. ¡Sigamos con el cuarto reto!"
            )
        );
    }

    private bool failedGame = false;
    private bool winGame = false;

    public bool CheckOption(string option, int currentQuestion)
    {
		
		if (option == currentOptions[currentQuestion].correctAnswer)
        {
			this.GetComponent<SoundManager> ().playSound ("Sound_correcto_10", 1);
			bool allQuestionsRigth = false;

            currentAnswers[currentQuestion] = true;
            int indexForAnswers;

            for (indexForAnswers = 0; indexForAnswers < currentAnswers.Length; indexForAnswers++)
            {
				if (!currentAnswers[indexForAnswers])
                {
                    break;
                }
            }

            if (indexForAnswers >= 3)
            {
                winGame = true;

            }
			numIntentos++;
			StartCoroutine (mostrarTrofeo ());
            return true;
        }
        else
        {
			this.GetComponent<SoundManager> ().playSound ("Sound_incorrecto_19", 1);

			if (numIntentos >= 2) {
				ShowMessageBox(currentOptions[currentQuestion].failText);
				failedGame = true;
			}
            return false;
        }
    }

	//Mostrar trofeo
	IEnumerator mostrarTrofeo()
	{
		TrofeoGameObject.SetActive (true);
		numTrofeos++;
		contadorGameObject.SetActive (true);
		contadorText.text = ""+numTrofeos;
		PlayerPrefs.SetInt ("oda10_trofeos",numTrofeos);
		yield return new WaitForSeconds (1.0f);
		TrofeoGameObject.SetActive (false);
		if (winGame) {
			this.GetComponent<SoundManager> ().playSound ("TerminarReto", 1);
			ShowMessageBox("Identificar tipos de textos es un paso importante para ser un gran crítico literario. ¡Sigamos con el  cuarto reto!");
		}
	}

    private void ShowMessageBox(string text)
    {
        popUpText.text = text;
        popUpContainer.SetActive(true);
    }

    private void HideMessageBox()
    {
		this.GetComponent<SoundManager> ().playSound ("Sound_Cerrar_3", 1);
		popUpContainer.SetActive(false);
        if (failedGame)
        {
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.Reto4);
            Menu.ParentObject.GetComponent<Reto4>().StartReto();
        }
        else if (winGame)
        {
            Menu.GoToScenarioStaticMethod(Menu.Scenarios.Reto4);
            Menu.ParentObject.GetComponent<Reto4>().StartReto();
        }
    }
}

public class OptionReto3
{
    public GameObject optionGameObject;
    public string correctAnswer;
    public string failText;

    public OptionReto3(GameObject optionGameObject, string correctAnswer, string failText)
    {
        this.optionGameObject = optionGameObject;
        this.correctAnswer = correctAnswer;
        this.failText = failText;
    }
}

public class DragMeReto3 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public bool dragOnSurfaces = false;
    public string CurrentAnswer = "";
    private Dictionary<int, GameObject> m_DraggingIcons = new Dictionary<int, GameObject>();
    private Dictionary<int, RectTransform> m_DraggingPlanes = new Dictionary<int, RectTransform>();

    public void OnBeginDrag(PointerEventData eventData)
    {
        var canvas = FindInParents<Canvas>(gameObject);
        if (canvas == null)
            return;

        // We have clicked something that can be dragged.
        // What we want to do is create an icon for this.
        m_DraggingIcons[eventData.pointerId] = new GameObject("icon");

        m_DraggingIcons[eventData.pointerId].transform.SetParent(canvas.transform, false);
        m_DraggingIcons[eventData.pointerId].transform.SetAsLastSibling();

        var image = m_DraggingIcons[eventData.pointerId].AddComponent<Image>();
        // The icon will be under the cursor.
        // We want it to be ignored by the event system.
        var group = m_DraggingIcons[eventData.pointerId].AddComponent<CanvasGroup>();
        group.blocksRaycasts = false;

        image.sprite = GetComponent<Image>().sprite;
        image.SetNativeSize();

        if (dragOnSurfaces)
            m_DraggingPlanes[eventData.pointerId] = transform as RectTransform;
        else
            m_DraggingPlanes[eventData.pointerId] = canvas.transform as RectTransform;

        SetDraggedPosition(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (m_DraggingIcons[eventData.pointerId] != null)
            SetDraggedPosition(eventData);
    }

    private void SetDraggedPosition(PointerEventData eventData)
    {
        if (dragOnSurfaces && eventData.pointerEnter != null && eventData.pointerEnter.transform as RectTransform != null)
            m_DraggingPlanes[eventData.pointerId] = eventData.pointerEnter.transform as RectTransform;

        var rt = m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>();
        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlanes[eventData.pointerId], eventData.position, eventData.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
            rt.rotation = m_DraggingPlanes[eventData.pointerId].rotation;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (m_DraggingIcons[eventData.pointerId] != null)
            Destroy(m_DraggingIcons[eventData.pointerId]);

        m_DraggingIcons[eventData.pointerId] = null;
    }

    static public T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        var t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }
}

public class DropMeReto3 : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    //public Image containerImage;
    public Image receivingImage;

    public int currentQuestion = 0;
    private Color normalColor;
    public Color highlightColor = Color.yellow;

    public void OnEnable()
    {
        //if (containerImage != null)
        //normalColor = containerImage.color;
    }

    public void OnDrop(PointerEventData data)
    {
        //containerImage.color = normalColor;

        if (receivingImage == null)
            return;

        Sprite dropSprite = GetDropSprite(data);
        if (dropSprite != null)
        {
            var originalObj = data.pointerDrag;
            string option = originalObj.GetComponent<DragMeReto3>().CurrentAnswer;
            
			if (receivingImage.color != Color.white) {
				if (Menu.ParentObject.GetComponent<Reto3>().CheckOption(option, currentQuestion))
				{
					receivingImage.overrideSprite = dropSprite;
					receivingImage.color = Color.white;
				}
			}
            
            /*else
            {
                receivingImage.sprite = null;
            }*/

            //receivingImage.overrideSprite = dropSprite;
            //print(dropSprite.name);
        }
    }

    public void OnPointerEnter(PointerEventData data)
    {
        //if (containerImage == null)
        //    return;

        //Sprite dropSprite = GetDropSprite(data);
        //if (dropSprite != null)
        //containerImage.color = highlightColor;
    }

    public void OnPointerExit(PointerEventData data)
    {
        //if (containerImage == null)
        //return;

        //containerImage.color = normalColor;
    }

    private Sprite GetDropSprite(PointerEventData data)
    {
        var originalObj = data.pointerDrag;
        if (originalObj == null)
            return null;

        var dragMe = originalObj.GetComponent<DragMeReto3>();
        if (dragMe == null)
            return null;

        var srcImage = originalObj.GetComponent<Image>();
        if (srcImage == null)
            return null;

        return srcImage.sprite;
    }
}