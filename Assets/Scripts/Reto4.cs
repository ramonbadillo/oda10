﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Reto4 : MonoBehaviour
{
    private InputField recomendacionInput;
    private Button ninaButton, buqueButton, aguaButton, nextButton1, nextButton2, saveButton;
	//Lista de stickers
	private List<Button> stickersButton = new List<Button> ();
    private Animator ninaAnimator, buqueAnimator, aguaAnimator;
	private Text recomiendo;

	//Contenedor Pantalla 1, 2, libros y stickers
	private GameObject Screen1, Screen2, libros, stickers; 
	//Stickers pegables
	private GameObject stickersImage1, stickersImage2, contadorGameObject;

	//PopUp Si/No pantalla 2
	private GameObject popUpScreen2, stickerSeleccion, popUpGeneral;
	private Button siButton, noButton;

	//PopUp General
	private Button popUpGeneralButton;
	private Text popUpText, contadorText;

	//Control reto
	int numPegatinasMostrar = 0;
	int numPegatinasUsar = 0;
	bool finReto = false;
	private int numTrofeos = 0;
	private int numTrofeosTemp = 0;

    // Use this for initialization
    private void Start()
    {
        //Elementos pantalla 1

		//GameObjects
		Screen1 = Menu.Reto4GameObject.transform.FindChild("Screen1").gameObject;
		libros = Screen1.transform.FindChild("libros").gameObject;
		stickers = Screen1.transform.FindChild("stickers").gameObject;
		contadorGameObject = Menu.Reto4GameObject.transform.FindChild("contador").gameObject;

		//Texto
		recomiendo = Screen1.transform.FindChild("Recomendacion").GetComponent<Text>();

		contadorText = contadorGameObject.transform.FindChild("contadorText").GetComponent<Text>();

		//Botones
		ninaButton = libros.transform.FindChild("NinaButton").GetComponent<Button>();
		buqueButton = libros.transform.FindChild("BuqueButton").GetComponent<Button>();
		aguaButton = libros.transform.FindChild("AguaButton").GetComponent<Button>();
		nextButton1 = Screen1.transform.FindChild("NextButton").GetComponent<Button>();
		saveButton = Screen1.transform.FindChild("SaveButton").GetComponent<Button>();

		//Animadores
		ninaAnimator = ninaButton.transform.GetChild(0).GetComponent<Animator>();
		buqueAnimator = buqueButton.transform.GetChild(0).GetComponent<Animator>();
		aguaAnimator = aguaButton.transform.GetChild(0).GetComponent<Animator>();

		//Imagenes arrastrables
		stickersImage1 = stickers.transform.FindChild("sticker1").gameObject;
		stickersImage2 = stickers.transform.FindChild("sticker2").gameObject;

		//Componentes arrastrable
		stickersImage1.AddComponent<DragReto4>();
		stickersImage2.AddComponent<DragReto4>();

		//InputField
		recomendacionInput = Screen1.transform.FindChild("InputField").GetComponent<InputField>();
        
		//Funciones Botones
        recomendacionInput.onValueChanged.AddListener(delegate{
        	CheckForEnableNext();
        });

        ninaButton.onClick.AddListener(delegate (){
            BookButtonMethod("ninia");
        });

        buqueButton.onClick.AddListener(delegate (){
        	BookButtonMethod("buque");
        });

        aguaButton.onClick.AddListener(delegate (){
            BookButtonMethod("agua");
        });

        nextButton1.onClick.AddListener(delegate (){
        	NextButonMethod();
        });

		saveButton.onClick.AddListener(delegate (){
			guardarMetodo();
		});

		//Elementos pantalla 2

		//GameObjects
		Screen2 = Menu.Reto4GameObject.transform.FindChild("Screen2").gameObject;
		popUpScreen2 = Screen2.transform.FindChild ("PopUpContainer").gameObject;

		//Botones
		stickersButton.Add(Screen2.transform.FindChild("sticker1").GetComponent<Button>());
		stickersButton.Add(Screen2.transform.FindChild("sticker2").GetComponent<Button>());
		stickersButton.Add(Screen2.transform.FindChild("sticker3").GetComponent<Button>());
		stickersButton.Add(Screen2.transform.FindChild("sticker4").GetComponent<Button>());
		nextButton2 = Screen2.transform.FindChild("NextButton").GetComponent<Button>();
		siButton = popUpScreen2.transform.FindChild ("SiButton").GetComponent<Button>();
		noButton = popUpScreen2.transform.FindChild ("NoButton").GetComponent<Button>();

		//Funciones Botones
		noButton.onClick.AddListener (delegate {
			popUpScreen2.SetActive(false);	
		});

		siButton.onClick.AddListener (delegate {
			aceptarSticker();	
		});

		stickersButton [0].onClick.AddListener (delegate {
			seleccionarStickers(stickersButton [0].gameObject);	
		});

		stickersButton [1].onClick.AddListener (delegate {
			seleccionarStickers(stickersButton [1].gameObject);	
		});

		stickersButton [2].onClick.AddListener (delegate {
			seleccionarStickers(stickersButton [2].gameObject);	
		});

		stickersButton [3].onClick.AddListener (delegate {
			seleccionarStickers(stickersButton [3].gameObject);	
		});

		nextButton2.onClick.AddListener (delegate() {
			ponerStickers();
			numTrofeos = numTrofeosTemp;
			PlayerPrefs.SetInt ("oda10_trofeos",numTrofeos);
		});

		//PopUp General
		popUpGeneral = Menu.Reto4GameObject.transform.FindChild("PopUpContainer").gameObject;
		popUpGeneralButton = popUpGeneral.transform.FindChild("Button").GetComponent<Button>();
		popUpText = popUpGeneral.transform.FindChild ("Text").GetComponent<Text> ();

		//Funciones Botones
		popUpGeneralButton.onClick.AddListener (delegate {
			cerrarPopUp();	
		});

        StartReto();
    }
		
    public void StartReto()
    {
        PlayAnimationOnBooks("shine");
		limpiarReto ();
		ShowMessageBox ("Para orientar a los futuros niños que entren  a la biblioteca y " +
			"no sepan qué libro escoger, redacta un pequeño texto para recomendarles uno " +
			"de los textos que acabas de leer. ");
    }

	void limpiarReto(){
		Screen1.SetActive (true);
		Screen2.SetActive (false);

		nextButton1.interactable = false;
		recomendacionInput.enabled = false;

		numPegatinasMostrar = 0;
		numPegatinasUsar = 0;

		recomiendo.text = "Recomiendo leer...";
		recomendacionInput.text = "";

		finReto = false;

		libros.SetActive (true);
		stickers.SetActive (false);
		limpiarStickers ();

		foreach (Button stickerButton in stickersButton) {
			stickerButton.gameObject.SetActive (false);
		}

		nextButton1.gameObject.SetActive (true);
		saveButton.gameObject.SetActive (false);

		if (PlayerPrefs.HasKey ("oda10_trofeos")) {
			numTrofeos = PlayerPrefs.GetInt ("oda10_trofeos");
		} else {
			numTrofeos = 0;
		}
		numTrofeosTemp = numTrofeos;
		contadorText.text = ""+numTrofeos;

		stickerSeleccion = null;
	}

	void limpiarStickers(){
	
		foreach (Transform sticker in stickers.transform) {
		
			if (sticker.name != "sticker1" && sticker.name != "sticker2") {
				Destroy (sticker.gameObject);
			}
		}
		stickersImage1.GetComponent<Image> ().sprite = null;
		stickersImage2.GetComponent<Image> ().sprite = null;
	}

	private void BookButtonMethod(string libro)
    {
        recomendacionInput.enabled = true;
        //Deja de brillar
		PlayAnimationOnBooks("default");

		//Muestra titulo del libro
		recomiendo.text = "Recomiendo leer ";
		switch (libro) 
		{
		case "ninia":
			recomiendo.text += "La niña que yo más quiero.";
			break;
		case "buque":
			recomiendo.text += "El buque fantasma.";
			break;
		case "agua":
			recomiendo.text += "Agua, ¿dónde vas?";
			break;
		}

		//Limpia input
		recomendacionInput.text = "";

		this.GetComponent<SoundManager> ().playSound ("Sound_SeleccionarRespuesta",1);
    }

	//Activar popUp general
	private void ShowMessageBox(string message)
	{
		this.GetComponent<SoundManager> ().playSound ("Sound_OpenPopUp_10",1);
		popUpGeneral.gameObject.SetActive(true);
		popUpText.text = message;
	}

	//Recomendacion lista, pegatinas
    private void NextButonMethod()
    {
		
		int trofeos = PlayerPrefs.GetInt ("oda10_trofeos");
		if (trofeos < 6) {
			ponerStickers ();
		} else {
			if (trofeos >= 9) {
				ShowMessageBox ("¡Felicidades! Puedes cambiar tus premios por 2 tipos de " +
					"pegatinas diferentes para adornar tu recomendación.");
				numPegatinasMostrar = 4;
				numPegatinasUsar = 2;
			} else if (trofeos == 8 || trofeos == 7) {
				ShowMessageBox ("¡Felicidades!  Puedes cambiar tus premios como crítico " +
					"literario por unas pegatinas para adornar tu recomendación.");
				numPegatinasMostrar = 2;
				numPegatinasUsar = 1;
			} else if (trofeos == 6) {
				ShowMessageBox ("¡Felicidades!  Puedes cambiar tus premios como crítico " +
					"literario por unas pegatinas para adornar tu recomendación.");
				numPegatinasMostrar = 1;
				numPegatinasUsar = 1;
			}
			mostrarStickers ();
		}
    }

	//Activar screen2 mostrar stickers random
	void mostrarStickers()
	{
		Screen1.SetActive (false);
		Screen2.SetActive (true);

		List<Button> stickersRandom = new List<Button> ();
		foreach (Button boton in stickersButton) {
			stickersRandom.Add(boton);
		}

		for (int index = 0; index < numPegatinasMostrar; index++) {
			int randomSticker = Random.Range (0, stickersRandom.Count);
			stickersRandom [randomSticker].gameObject.SetActive (true);
			stickersRandom.RemoveAt (randomSticker);
		}
	}

	//Botones de stickers
	void seleccionarStickers(GameObject stickerSeleccionado)
	{
		stickerSeleccion = stickerSeleccionado;
		if (numPegatinasUsar == 0) {
			ponerStickers ();
			numTrofeos = numTrofeosTemp;
			PlayerPrefs.SetInt ("oda10_trofeos",numTrofeos);
		} else {
			popUpScreen2.SetActive (true);
		}
	}

	//Boton si de pop up screen2
	void aceptarSticker()
	{
		
		popUpScreen2.SetActive (false);

		numTrofeosTemp = numTrofeosTemp - 4;
		contadorText.text = ""+numTrofeosTemp;

		switch (numPegatinasUsar) 
		{
		case 2:	
			stickersImage2.GetComponent<Image> ().sprite = 
				stickerSeleccion.GetComponent<Image> ().sprite;
			break;
		case 1:	
			stickersImage1.GetComponent<Image> ().sprite = 
				stickerSeleccion.GetComponent<Image> ().sprite;
			break;
		}
		numPegatinasUsar--;
		if (numPegatinasUsar == 0) {
			ponerStickers ();
			numTrofeos = numTrofeosTemp;
			PlayerPrefs.SetInt ("oda10_trofeos",numTrofeos);
		}
	}

	void ponerStickers(){


		Screen1.SetActive (true);
		Screen2.SetActive (false);
	
		libros.SetActive (false);

		nextButton1.gameObject.SetActive (false);
		saveButton.gameObject.SetActive (true);

		stickers.SetActive (true);
		for (int numStickers = 0; numStickers < 4; numStickers++) {
			if (stickersImage1.GetComponent<Image> ().sprite != null) {
				GameObject clone = Instantiate (stickersImage1);
				clone.SetActive (true);
				clone.transform.SetParent (stickersImage1.transform.parent);
				clone.transform.localScale = new Vector3 (1.0f,1.0f,1.0f);
				clone.transform.localPosition = stickersImage1.transform.localPosition;
				clone.GetComponent<Image> ().SetNativeSize ();
				clone.GetComponent<Image> ().color = Color.white;
			}
			if (stickersImage2.GetComponent<Image> ().sprite != null) {
				GameObject clone2 = Instantiate (stickersImage2);
				clone2.SetActive (true);
				clone2.transform.SetParent (stickersImage2.transform.parent);
				clone2.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
				clone2.transform.localPosition = stickersImage2.transform.localPosition;
				clone2.GetComponent<Image> ().SetNativeSize ();
				clone2.GetComponent<Image> ().color = Color.white;
			}
		}
	}

	void guardarMetodo(){

		StartCoroutine(ScreenshotEncode());
	
		ShowMessageBox ("¡Sigamos con el reto final!");
		finReto = true;

	}

	void cerrarPopUp(){
		this.GetComponent<SoundManager> ().playSound ("Sound_Cerrar_3",1);
		popUpGeneral.gameObject.SetActive(false);
		if (finReto) {
			limpiarReto ();
			this.GetComponent<SoundManager> ().playSound ("TerminarReto",1);
			Menu.GoToScenarioStaticMethod(Menu.Scenarios.RetoFinal);
			gameObject.GetComponent<RetoFinal>().StartReto();
		}
	}

	//Anima los libros
    private void PlayAnimationOnBooks(string animName)
    {
        ninaAnimator.Play(animName);
        buqueAnimator.Play(animName);
        aguaAnimator.Play(animName);
    }

	//Checa que escriba al menos 20 caracteres
    private void CheckForEnableNext()
    {
        if (recomendacionInput.text.Length == 20)
        {
            nextButton1.interactable = true;
        }
    }

    // Update is called once per frame
    private void Update()
    {

	}

	//----------------Screen shot script ---------------------//

	Texture2D texture1;
	private static AndroidJavaObject _activity;

	private const string MediaStoreImagesMediaClass = "android.provider.MediaStore$Images$Media";

	public static string SaveImageToGallery(Texture2D texture2D, string title, string description)
	{
		using (var mediaClass = new AndroidJavaClass(MediaStoreImagesMediaClass))
		{
			using (var cr = Activity.Call<AndroidJavaObject>("getContentResolver"))
			{
				var image = Texture2DToAndroidBitmap(texture2D);
				var imageUrl = mediaClass.CallStatic<string>("insertImage", cr, image, title, description);
				return imageUrl;
			}
		}
	}

	public static AndroidJavaObject Texture2DToAndroidBitmap(Texture2D texture2D)
	{
		byte[] encoded = texture2D.EncodeToPNG();
		using (var bf = new AndroidJavaClass("android.graphics.BitmapFactory"))
		{
			return bf.CallStatic<AndroidJavaObject>("decodeByteArray", encoded, 0, encoded.Length);
		}
	}

	public static AndroidJavaObject Activity
	{
		get
		{
			if (_activity == null)
			{
				var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				_activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
			}
			return _activity;
		}
	}


	IEnumerator ScreenshotEncode() {

		yield return new WaitForEndOfFrame ();
		texture1 = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);
		texture1.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);
		texture1.Apply ();
		SaveImageToGallery (texture1, "CapturaOda1", "");
	}

}

public class DragReto4 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{


	public void OnBeginDrag(PointerEventData eventData)
	{

		//Se agrupa con el cursor
		if(gameObject.GetComponent<CanvasGroup>() == null){
			gameObject.AddComponent<CanvasGroup>();
		}

		//Se indica que no estorbe los ray casts
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

	}

	public void OnDrag(PointerEventData eventData)
	{
		Vector3 globalMousePos;
		RectTransformUtility.ScreenPointToWorldPointInRectangle (
			eventData.pointerEnter.transform as RectTransform, eventData.position, 
			eventData.pressEventCamera, out globalMousePos
		);
		transform.position = globalMousePos;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
	}
}