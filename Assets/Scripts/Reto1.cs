﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Reto1 : MonoBehaviour
{
    private GameObject kidContainerGameObject, bibliotecariaGameObject;
    private List<Button> kidsButtons = new List<Button>();
    private List<GameObject> bookGameObjects = new List<GameObject>();
    private GameObject soldadoBook, buqueBook, amigaBook, ninaBook, aguaBook, abolesBook, bookPrefabContainer;
    private Button skipButton, nextStepButton, popUpContainerButton;
	private GameObject popUpContainer;

    private List<QuestionReto1> Step1 = new List<QuestionReto1>();
    private List<QuestionReto1> Step2 = new List<QuestionReto1>();
    private List<QuestionReto1> Step3 = new List<QuestionReto1>();
    private List<QuestionReto1> CurrentSteps = new List<QuestionReto1>();

    private GameObject Screen1GameObject, Screen2GameObject, firstBookPosition, secondBookPosition, thirdBookPosition;
    private Text popUpText, contadorText;
    private GameObject TrofeoGameObject, contadorGameObject;

    //Animation Text Elements
    private Text animatedText;

    private GameObject globePrefab;
    private int secondKidIndex, thirdKidIndex;
    private int characterIndex = 0;
    private int currentKid = 0;
    private int numberOfTrys = 0;

	private int numTrofeos = 0;

    // Use this for initialization
    private void Start()
    {
        Screen2GameObject = Menu.Reto1GameObject.transform.GetChild(1).gameObject;
        Screen1GameObject = Menu.Reto1GameObject.transform.GetChild(0).gameObject;
        firstBookPosition = Screen2GameObject.transform.FindChild("FirstBookPosition").gameObject;
        secondBookPosition = Screen2GameObject.transform.FindChild("SecondBookPosition").gameObject;
        thirdBookPosition = Screen2GameObject.transform.FindChild("ThirdBookPosition").gameObject;
		popUpContainer = Screen2GameObject.transform.FindChild ("PopUpContainer").gameObject;
		popUpContainerButton = popUpContainer.transform.FindChild("Button").GetComponent<Button>();
		popUpText = popUpContainer.transform.GetChild(1).GetComponent<Text>();

        TrofeoGameObject = Screen1GameObject.transform.FindChild("Trofeo").gameObject;
		contadorGameObject = Screen1GameObject.transform.FindChild ("contador").gameObject;
		contadorText = contadorGameObject.transform.FindChild("contadorText").GetComponent<Text>();

        bookPrefabContainer = Screen2GameObject.transform.FindChild("BookPrefabContainer").gameObject;
        soldadoBook = bookPrefabContainer.transform.FindChild("Soldado").gameObject;
        buqueBook = bookPrefabContainer.transform.FindChild("Buque").gameObject;
        amigaBook = bookPrefabContainer.transform.FindChild("Amiga").gameObject;
        ninaBook = bookPrefabContainer.transform.FindChild("Nina").gameObject;
        aguaBook = bookPrefabContainer.transform.FindChild("Agua").gameObject;
        abolesBook = bookPrefabContainer.transform.FindChild("Arboles").gameObject;

        nextStepButton = Menu.Reto1GameObject.transform.GetChild(0).FindChild("nextStepButton").GetComponent<Button>();
        skipButton = Menu.Reto1GameObject.transform.GetChild(0).FindChild("SkipButton").GetComponent<Button>();
        globePrefab = Menu.Reto1GameObject.transform.GetChild(0).FindChild("GlobePrefab").gameObject;
        bibliotecariaGameObject = Menu.Reto1GameObject.transform.GetChild(0).FindChild("BibliotecariaGlobePosition").gameObject;
        animatedText = globePrefab.transform.GetChild(0).GetComponent<Text>();
        kidContainerGameObject = Menu.Reto1GameObject.transform.GetChild(0).FindChild("Kids").gameObject;

        foreach (Transform currentBook in bookPrefabContainer.transform)
        {
            GameObject currentGameObject = currentBook.gameObject;
            currentBook.GetComponent<Button>().onClick.AddListener(delegate ()
            {
                BookButtonMethod(currentGameObject);
            });

            bookGameObjects.Add(currentBook.gameObject);
        }

        foreach (Transform curentKid in kidContainerGameObject.transform)
        {
            kidsButtons.Add(curentKid.GetComponent<Button>());
        }

        foreach (Button currentButton in kidsButtons)
        {
            currentButton.gameObject.SetActive(false);
            currentButton.interactable = false;
            currentButton.onClick.AddListener(delegate ()
            {
                KidButtonMethod();
            });
        }

        popUpContainerButton.onClick.AddListener(delegate ()
        {
            PopUpMethod();
        });

        nextStepButton.onClick.AddListener(delegate ()
        {
            NextButtonMethod();
        });

        skipButton.onClick.AddListener(delegate ()
         {
             SkipButtonMethod();
         });

        FillQuestions();

        //StartReto();
    }

    private void GetRandomKids()
    {
        secondKidIndex = Random.Range(1, 3);
        if (secondKidIndex == 1)
        {
            thirdKidIndex = 2;
        }
        else
        {
            thirdKidIndex = 1;
        }
    }

    private void NextButtonMethod()
    {
        currentStepIndex++;
        MainMethod();
        nextStepButton.gameObject.SetActive(false);
    }

    public void CleanReto()
    {
        foreach (Button currenButton in kidsButtons)
        {
            currenButton.gameObject.SetActive(false);
        }
        Screen2GameObject.SetActive(false);
        CurrentSteps.Clear();
        globePrefab.SetActive(false);
		contadorGameObject.gameObject.SetActive (false);
    }

    public void StartReto()
    {
        CleanReto();

        CurrentSteps.Add(Step1[Random.Range(0, 2)]);
        CurrentSteps.Add(Step2[Random.Range(0, 2)]);
        CurrentSteps.Add(Step3[Random.Range(0, 2)]);
        currentKid = 0;
        GetRandomKids();
        numberOfTrys = 0;
        currentStepIndex = 0;
		if (PlayerPrefs.HasKey ("oda10_trofeos")) {
			numTrofeos = PlayerPrefs.GetInt ("oda10_trofeos");
		} else {
			numTrofeos = 0;
		}
		contadorText.text = ""+numTrofeos;

        MainMethod();

    }

    private int currentStepIndex = 0;

    public void MainMethod()
    {
        switch (currentStepIndex)
        {
            case 0:

                StartCoroutine(DisplayTimer(bibliotecariaGameObject.transform, "Ayuda a los niños a escoger un libro de acuerdo con sus gustos.", 0.15f));

                break;

            case 1:
                nextStepButton.gameObject.SetActive(true);

                break;

            case 2:

                foreach (Button currenButton in kidsButtons)
                {
                    currenButton.gameObject.SetActive(true);
                }
                StartShine(currentKid);
                kidsButtons[currentKid].interactable = true;
                break;

            case 3:
                StartCoroutine(DisplayTimer(kidsButtons[currentKid].transform.GetChild(1), CurrentSteps[currentKid].QuestionText, 0.15f));
				StopAllShines();
                break;

            case 4:
                
                nextStepButton.gameObject.SetActive(true);
                break;

            case 5:
                ShowQuestion(CurrentSteps[0]);
                globePrefab.SetActive(false);
                break;

            case 6:
				currentKid = secondKidIndex;
                StartShine(currentKid);
                kidsButtons[currentKid].interactable = true;
                break;

            case 7:
                StartCoroutine(DisplayTimer(kidsButtons[currentKid].transform.GetChild(1), CurrentSteps[currentKid].QuestionText, 0.15f));
				StopAllShines();    
				break;

            case 8:
                
                nextStepButton.gameObject.SetActive(true);
                break;

            case 9:
                ShowQuestion(CurrentSteps[currentKid]);
                globePrefab.SetActive(false);
                break;

            case 10:
                currentKid = thirdKidIndex;
                StartShine(currentKid);
                kidsButtons[currentKid].interactable = true;
                break;

            case 11:
                StartCoroutine(DisplayTimer(kidsButtons[currentKid].transform.GetChild(1), CurrentSteps[currentKid].QuestionText, 0.15f));
				StopAllShines();
				break;

            case 12:
                
                nextStepButton.gameObject.SetActive(true);
                break;

            case 13:
                ShowQuestion(CurrentSteps[currentKid]);
                globePrefab.SetActive(false);
                break;
        }
    }

    private void HideAllBooks()
    {
        foreach (GameObject currentBook in bookGameObjects)
        {
            currentBook.transform.SetParent(bookPrefabContainer.transform);
            currentBook.transform.localPosition = Vector3.zero;
        }
    }

    private void ShowQuestion(QuestionReto1 currentQuestion)
    {
        Menu.ParentObject.GetComponent<SoundManager>().playSound("Sound_OpenPopUp_10", 1);
        HideAllBooks();
        currentQuestion.OptionList[0].transform.SetParent(firstBookPosition.transform);
        currentQuestion.OptionList[0].transform.localPosition = Vector3.zero;
        currentQuestion.OptionList[1].transform.SetParent(secondBookPosition.transform);
        currentQuestion.OptionList[1].transform.localPosition = Vector3.zero;
        currentQuestion.OptionList[2].transform.SetParent(thirdBookPosition.transform);
        currentQuestion.OptionList[2].transform.localPosition = Vector3.zero;
        Screen2GameObject.SetActive(true);
    }

    private void StartShine(int indexAnimation)
    {
        StopAllShines();
        kidsButtons[indexAnimation].transform.GetChild(0).GetComponent<Animator>().Play("shine");
    }

    private void StopAllShines()
    {
        for (int i = 0; i < kidsButtons.Count; i++)
        {
            kidsButtons[i].interactable = false;
            kidsButtons[i].transform.GetChild(0).GetComponent<Animator>().Play("default");
        }
    }

    private string fullAnimatedText = "";

    private IEnumerator DisplayTimer(Transform positionTransform, string text, float speed)
    {
        skipButton.gameObject.SetActive(true);
        fullAnimatedText = text;
        globePrefab.transform.SetParent(positionTransform);
        globePrefab.transform.localScale = Vector3.one;
        globePrefab.transform.localPosition = Vector3.zero;
        globePrefab.SetActive(true);
        animatedText.text = "";
        globePrefab.SetActive(true);
        characterIndex = 0;
        while (true)
        {
            yield return new WaitForSeconds(speed);
            if (characterIndex > text.Length)
            {
                currentStepIndex++;
                MainMethod();
				skipButton.gameObject.SetActive(false);
                break;
            }
            animatedText.text = text.Substring(0, characterIndex);
            characterIndex++;
        }
    }

    private void SkipButtonMethod()
    {
        characterIndex = fullAnimatedText.Length;
        animatedText.text = fullAnimatedText;
        skipButton.gameObject.SetActive(false);
    }

    private void KidButtonMethod()
    {
        currentStepIndex++;
        MainMethod();
    }

    private void BookButtonMethod(GameObject bookGameObject)
    {
        numberOfTrys++;
        popUpContainer.SetActive(true);
        switch (numberOfTrys)
        {
            case 1:
                if (bookGameObject.name == CurrentSteps[currentKid].AnswerGameObject.name)
                {
                    popUpText.text = CurrentSteps[currentKid].FirstTryWinText;
                    numberOfTrys = 0;
					this.GetComponent<SoundManager> ().playSound ("Sound_correcto_10", 1);
                }
                else
                {
                    popUpText.text = CurrentSteps[currentKid].FirstTryFailText;
					this.GetComponent<SoundManager> ().playSound ("Sound_incorrecto_19", 1);
                }

                break;

            case 2:
                if (bookGameObject.name == CurrentSteps[currentKid].AnswerGameObject.name)
                {
                    popUpText.text = CurrentSteps[currentKid].SecondTryWinText;
                    numberOfTrys = 0;
					this.GetComponent<SoundManager> ().playSound ("Sound_correcto_10", 1);
                }
                else
                {
                    popUpText.text = CurrentSteps[currentKid].SecondTryFailText;
					this.GetComponent<SoundManager> ().playSound ("Sound_incorrecto_19", 1);
                }
                break;
        }
    }

    private void PopUpMethod()
    {
		this.GetComponent<SoundManager> ().playSound ("Sound_cerrar_3", 1);
		switch (numberOfTrys)
        {
            case 0:
				StartCoroutine (mostrarTrofeo());
                break;

            case 1:
                popUpContainer.SetActive(false);
                break;

            case 2:
				StartCoroutine (continuarReto());
                break;

			case 6:
				PlayerPrefs.SetInt ("oda10_trofeos", numTrofeos);
				popUpContainer.SetActive (false);
                Menu.GoToScenarioStaticMethod(Menu.Scenarios.Reto2);
                gameObject.GetComponent<Reto2>().StartReto();
                break;
        }
    }

	//Mostrar trofeo
	IEnumerator mostrarTrofeo()
	{
		popUpContainer.SetActive(false);
		Screen2GameObject.SetActive(false);
		TrofeoGameObject.SetActive (true);
		numTrofeos++;
		contadorGameObject.gameObject.SetActive (true);
		contadorText.text = ""+numTrofeos;
		yield return new WaitForSeconds (1.0f);
		TrofeoGameObject.SetActive (false);
		if (currentStepIndex == 13)
		{
			this.GetComponent<SoundManager> ().playSound ("TerminarReto", 1);
			popUpContainer.SetActive(true);
			popUpText.text = "¡Sigamos con el segundo reto!";
			numberOfTrys = 6;
		}
		else
		{
			currentStepIndex++;
		}

		MainMethod ();
	}

	//Mostrar trofeo
	IEnumerator continuarReto()
	{
		numberOfTrys = 0;
		popUpContainer.SetActive(false);
		Screen2GameObject.SetActive(false);
		contadorGameObject.gameObject.SetActive (true);
		contadorText.text = ""+numTrofeos;
		yield return new WaitForSeconds (0.01f);
		if (currentStepIndex == 13)
		{
			this.GetComponent<SoundManager> ().playSound ("TerminarReto", 1);
			popUpContainer.SetActive(true);
			popUpText.text = "¡Sigamos con el segundo reto!";
			numberOfTrys = 6;
		}
		else
		{
			currentStepIndex++;
		}

		MainMethod ();
	}

    private void FillQuestions()
    {
        ///Opciones del primer ninio
        Step1.Add(
            new QuestionReto1(
                "¡Me encantan las historias que hablan de la naturaleza! ¿Cuál libro me recomiendas?",
                new GameObject[] { buqueBook, aguaBook, ninaBook },
                aguaBook,
                "¡Correcto! Acabas de ganarte tu primer premio como crítico literario. ",
                "El título no está relacionado con la naturaleza, prueba con uno que puede relacionarse con elementos naturales.",
                "¡Correcto! Acabas de ganarte tu primer premio como crítico literario. ",
                "El agua es un elemento que forma parte de la naturaleza, había que recomendar Agua, ¿dónde vas?"
                )
            );
        Step1.Add(
            new QuestionReto1(
                "¡Me encantan las historias que hablan de la amistad! ¿Cuál libro me recomiendas?",
                new GameObject[] { soldadoBook, buqueBook, amigaBook },
                amigaBook,
                "¡Correcto! Acabas de ganarte tu primer premio como crítico literario. ",
                "El título  no está relacionado con la amistad, prueba con uno que puede relacionarse con el afecto entre dos personas.",
                "¡Correcto! Acabas de ganarte tu primer premio como crítico literario. ",
                "A una amiga se le tiene afecto y cariño, entre dos amigas hay amistad, había que recomendar Mi mejor amiga."
                )
            );

        //opciones del segundo ninio
        Step2.Add(
            new QuestionReto1(
                "A mí…  ¡Me encantan las historias misteriosas! ¿Cuál libro me recomiendas?",
                new GameObject[] { buqueBook, soldadoBook, ninaBook },
                buqueBook,
                "¡Correcto! Un buque fantasma es un barco habitado por fantasmas, ¡esto es muy misterioso!",
                "El título no está relacionado con algo misterioso, prueba con uno que puede relacionarse con sucesos extraños.",
                "¡Correcto! Un buque fantasma es un barco habitado por fantasmas, ¡esto es muy misterioso!",
                "Un buque fantasma es un barco habitado por fantasmas, ¡esto es muy misterioso!, había que recomendar El buque fantasma."
                )
            );
        Step2.Add(
            new QuestionReto1(
                "A mí… ¡Me encanta leer sobre cosas de la naturaleza. ¿Cuál libro me recomiendas?",
                new GameObject[] { soldadoBook, abolesBook, ninaBook },
                abolesBook,
                "¡Correcto! Los árboles forman parte de la naturaleza.",
                "El título no está relacionado con la naturaleza, prueba con uno que puede relacionarse con elementos naturales.",
                "¡Correcto! Los árboles forman parte de la naturaleza.",
                "Los árboles forman parte de la naturaleza, había que recomendar Árboles."
                )
            );

        //opciones del tercer ninio
        Step3.Add(
            new QuestionReto1(
                "Yo, prefiero las historias de amor.  ¿Cuál libro me recomiendas?",
                new GameObject[] { abolesBook, soldadoBook, ninaBook },
                ninaBook,
                "¡Correcto! El verbo “querer”  da a pensar que este texto va a hablar de amor.",
                "El título no está relacionado con el amor, prueba con uno que puede relacionarse con los sentimientos amorosos.",
                "¡Correcto! El verbo “querer”  da a pensar que este texto va a hablar de amor.",
                "El verbo “querer”  da a pensar que este texto va a hablar de amor, había que recomendar La niña que yo más quiero."
                )
            );
        Step3.Add(
            new QuestionReto1(
                "Yo, prefiero las historias sobre la guerra. ¿Cuál libro me recomiendas?",
                new GameObject[] { soldadoBook, buqueBook, ninaBook },
                soldadoBook,
                "¡Correcto! Los soldados se relacionan generalmente con la guerra.",
                "El título no está relacionado con la guerra, prueba con uno que puede relacionarse con conflictos armados.",
                "¡Correcto! Los soldados se relacionan generalmente con la guerra. ",
                "Los soldados se relacionan generalmente con la guerra, había que recomendar El regreso de un soldado."
                )
            );
    }
}

public class QuestionReto1
{
    public string QuestionText;
    public GameObject[] OptionList;
    public GameObject AnswerGameObject;
    public string FirstTryWinText;
    public string FirstTryFailText;
    public string SecondTryWinText;
    public string SecondTryFailText;

    public QuestionReto1(string QuestionText, GameObject[] OptionList, GameObject AnswerGameObject, string FirstTryWinText, string FirstTryFailText, string SecondTryWinText, string SecondTryFailText)
    {
        this.QuestionText = QuestionText;
        this.OptionList = OptionList;
        this.AnswerGameObject = AnswerGameObject;
        this.FirstTryWinText = FirstTryWinText;
        this.FirstTryFailText = FirstTryFailText;
        this.SecondTryWinText = SecondTryWinText;
        this.SecondTryFailText = SecondTryFailText;
    }
}